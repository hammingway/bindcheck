﻿#include <array>
#include <sstream>
#include "database.h"
#include "hide_string.h"

static const std::array<std::wregex, 7> patterns = {
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{3})(\\d{2})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{3})(\\d{4})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})\\s*(\\d{2})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})\\s*(\\d{2})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})\\s*(\\d{4})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})\\s*(\\d{4})\\s*$"),
	std::wregex(L"^\\s*([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})"
				L"\\s+([ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ-]{2,})\\s*$") };

DEFINE_HIDDEN_STRING(HiddenPragma, 0x15, ('P')('R')('A')('G')('M')('A')(' ')('k')('e')('y'))
DEFINE_HIDDEN_STRING(HiddenKey, 0x10, ('e')('m')('p')('t')('y'))

Database::Database(const std::wstring &name) : db(utf8ws.to_bytes(name).c_str())
{
	db.execute((std::string(GetHiddenPragma()) + '=' + GetHiddenKey()).c_str());
}

bool Database::pattern_get(Match &match, const std::wstring &str)
{
	unsigned idx = 0;
	for (auto &pattern : patterns)
	{
		std::wsmatch mo;
		if (std::regex_match(str, mo, pattern)) {
			match.mode = idx + 1;
			match.match.clear();
			match.match.insert(match.match.begin(), mo.begin() + 1, mo.end());
			return true;
		}
		++idx;
	}
	return false;
}

std::wstring Database::query_prep(const Match &match)
{
	std::wostringstream ss;
	switch (match.mode) {
	case 1: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0][0] << "%\""
		<< " and im like "
		<< "\"" << match.match[0][1] << "%\""
		<< " and ot like "
		<< "\"" << match.match[0][2] << "%\""
		<< " and drdy like "
		<< "\"%" << match.match[1] << "\"";
		break;
	case 2: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0][0] << "%\""
		<< " and im like "
		<< "\"" << match.match[0][1] << "%\""
		<< " and ot like "
		<< "\"" << match.match[0][2] << "%\""
		<< " and drdy = "
		<< "\"" << match.match[1] << "\"";
		break;
	case 3: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0] << "%\""
		<< " and im like "
		<< "\"" << match.match[1] << "%\""
		<< " and drdy like "
		<< "\"%" << match.match[2] << "\"";
		break;
	case 4: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0] << "%\""
		<< " and im like "
		<< "\"" << match.match[1] << "%\""
		<< " and ot like "
		<< "\"" << match.match[2] << "%\""
		<< " and drdy like "
		<< "\"%" << match.match[3] << "\"";
		break;
	case 5: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0] << "%\""
		<< " and im like "
		<< "\"" << match.match[1] << "%\""
		<< " and drdy = "
		<< "\"" << match.match[2] << "\"";
		break;
	case 6: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0] << "%\""
		<< " and im like "
		<< "\"" << match.match[1] << "%\""
		<< " and ot like "
		<< "\"" << match.match[2] << "%\""
		<< " and drdy = "
		<< "\"" << match.match[3] << "\"";
		break;
	case 7: ss
		<< "select n6, fam, im, ot, drd, lnkd from patient where fam like "
		<< "\"" << match.match[0] << "%\""
		<< " and im like "
		<< "\"" << match.match[1] << "%\""
		<< " and ot like "
		<< "\"" << match.match[2] << "%\"";
		break;
	default:;
	}
	return ss.str();
}

dataset_type Database::fetch(const std::wstring &query)
{
	dataset_type data;
	sqlite3pp::query qry(db, utf8ws.to_bytes(query).c_str());
	for (auto &row : qry)
	{
		row_type rowd;
		for (int i = 0; i < qry.column_count(); ++i) {
			rowd.emplace_back(utf8ws.from_bytes(row.get<char const*>(i)));
		}
		data.emplace_back(rowd);
	}
	return data;
}