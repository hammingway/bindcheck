﻿#pragma once

#include <memory>
#include <wx/imaglist.h>
#include "form.h"
#include "database.h"

namespace gui {

    class MainForm : public MainFormBase {
        std::unique_ptr<Database> db;
        std::wstring_convert<std::codecvt_utf8<wchar_t>> utf8ws;
    public:
        MainForm(wxWindow* parent);
        ~MainForm();

    protected:
        virtual void onClose(wxCloseEvent& event) override;
        virtual void onInit(wxInitDialogEvent& event) override;
		virtual void onTextUpdated(wxCommandEvent& event) override;
        virtual void onEnterPressed(wxCommandEvent& event) override { doSearch(); }
        virtual void onSearchClick(wxCommandEvent& event) override { doSearch(); }

        void doSearch();
    };
}
