﻿#include <wx/app.h>
#include <wx/event.h>
#include <wx/intl.h>
#include <wx/image.h>
#include <wx/msgdlg.h>
#include "main.h"
#include "hide_string.h"

DEFINE_HIDDEN_STRING(HiddenDBName, 0x11, ('d')('a')('t')('a')('.')('c')('d')('b'))

static const wchar_t *column_names[] = 
    { L"Полис", L"Фамилия", L"Имя", L"Отчество", L"Родился", L"Прикреплён" };

gui::MainForm::MainForm(wxWindow* parent) : MainFormBase(parent)
{
}

gui::MainForm::~MainForm()
{
}

void gui::MainForm::onClose(wxCloseEvent& event)
{
    Destroy();
}

void gui::MainForm::onInit(wxInitDialogEvent& event)
{
	SetTitle(L"Поиск прикреплённых");
	{
		size_t i = 0;
		for (const auto &cn : column_names) {
			m_grid1->SetColLabelValue(i, cn);
			++i;
		}
	}
	try {
		db = std::make_unique<Database>(utf8ws.from_bytes(GetHiddenDBName()));
	}
	catch (std::exception ex) {
		wxMessageBox(ex.what(), wxT("Error"), wxICON_ERROR, this);
	}
}

void gui::MainForm::onTextUpdated(wxCommandEvent& event)
{
	m_textCtrl1->SetForegroundColour(*wxBLACK);
	m_textCtrl1->Refresh();
}

void gui::MainForm::doSearch()
{
    Match match;
	SetCursor(wxCURSOR_WAIT);
    m_grid1->BeginBatch();
    m_grid1->DeleteRows(0, m_grid1->GetNumberRows());
    if (db->pattern_get(match, m_textCtrl1->GetValue().Upper().wc_str())) {
		auto ifont = m_textCtrl1->GetFont();
        try {
            dataset_type data = db->fetch(db->query_prep(match));
            m_grid1->AppendRows(data.size());
            for (size_t i = 0; i < data.size(); ++i)
            {
                for (size_t j = 0; j < data[i].size(); ++j) {
                    m_grid1->SetCellValue(i, j, data[i][j]);
                    auto font = m_grid1->GetCellFont(i, j);
                    font.SetPointSize(ifont.GetPointSize());
                    font.SetFamily(wxFONTFAMILY_TELETYPE);
                    m_grid1->SetCellFont(i, j, font);
                }
            }
            m_grid1->AutoSizeColumns();
            m_grid1->AutoSizeRows();
			m_textCtrl1->SelectAll();
        }
        catch (std::exception& ex) {
            wxMessageBox(wxT("Exception during select is here!"), wxT("Error"), wxICON_ERROR, this);
            //wxMessageBox(ex.what(), wxT("Error"), wxICON_ERROR, this);
        }
	}
	else {
		m_textCtrl1->SetForegroundColour(*wxRED);
		m_textCtrl1->Refresh();
		wxMessageBox(L"Неверный формат запроса!", wxT("Error"), wxICON_ERROR, this);
	}
    m_grid1->EndBatch();
	SetCursor(wxCURSOR_ARROW);
}

class MainApp : public wxApp
{
    wxLocale locale;
public:
    MainApp() {}
    virtual ~MainApp() {}
    virtual bool OnInit();
};

DECLARE_APP(MainApp)
IMPLEMENT_APP(MainApp)

bool MainApp::OnInit() {
    if (locale.Init(wxLANGUAGE_DEFAULT, wxLOCALE_LOAD_DEFAULT | wxLOCALE_CONV_ENCODING)) {
        locale.AddCatalog(wxT("bindcheck"));
    }
    wxInitAllImageHandlers();
    auto mainWindow = new gui::MainForm(NULL);
    mainWindow->Show(true);
    return true;
}
