﻿#pragma once

#include <string>
#include <regex>
#include <locale>
#include <codecvt>
#include <vector>
#include "sqlite3pp.h"

using row_type = std::vector<std::wstring>;
using dataset_type = std::vector<row_type>;

struct Match {
    int mode;
    std::vector<std::wstring> match;
};

class Database {
    std::wstring_convert<std::codecvt_utf8<wchar_t>> utf8ws;
    sqlite3pp::database db;
public:
    Database(const std::wstring &name);
    bool pattern_get(Match &match, const std::wstring &str);
    std::wstring query_prep(const Match &match);
    dataset_type fetch(const std::wstring &query);
};